defmodule Loany.UserLoanInterfaceTest do
  @moduledoc false

  use Loany.DataCase, async: false

  alias Loany.Models.UserLoan
  alias Loany.UserLoanInterface, as: Interface

  setup do
    payload = %{
      names: "Miguel Diaz",
      email: "dummy@email.com",
      phone_number: "+57123123",
      req_loan_amount: 10000,
      interest_rate: 4.0,
      approval_status: "approved"
    }

    start_supervised!(Loany.LoansRecorder, [])

    File.rm("recorded_loans_test")

    {:ok, params: payload}
  end

  test "returns changeset for user loan" do
    assert %Ecto.Changeset{} = Interface.change_user_loan(%{})
  end

  test "inserts data on database and process' memory", %{params: params} do
    loan =
      %UserLoan{}
      |> Ecto.Changeset.change(params)
      |> Interface.create_loan_request()

    assert {:ok, %UserLoan{}} = loan

    loan_state = :sys.get_state(:loans_recorder)

    assert loan_state.loans_recorded == [params.req_loan_amount]
  end

  test "filters loans by their status", %{params: params} do
    # Just so we can wait for the operation to go through.
    # We can use Repo.insert_all() but this way there's less boilerplate.
    Enum.map(["approved", "denied"], fn status ->
      new_params = %{params | approval_status: status}

      %UserLoan{}
      |> Ecto.Changeset.change(new_params)
      |> Interface.create_loan_request()
    end)

    assert length(Interface.get_all_loans("all")) == 2
    assert length(Interface.get_all_loans("denied")) == 1
    assert length(Interface.get_all_loans("approved")) == 1
  end
end
