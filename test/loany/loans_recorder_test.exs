defmodule Loany.LoansRecorderTest do
  @moduledoc false

  use Loany.DataCase, async: false

  alias Loany.LoansRecorder

  setup_all do
    File.rm("recorded_loans_test")

    :ok
  end

  setup do
    start_supervised!(Loany.LoansRecorder, [])

    :ok
  end

  test "returns whether a loan is the lowest or not" do
    :sys.replace_state(:loans_recorder, fn state ->
      %{state | loans_recorded: [10000, 34_000_000]}
    end)

    refute LoansRecorder.is_lowest_loan?(40_000_000)
    assert LoansRecorder.is_lowest_loan?(5000)
  end

  test "records a loan when it meets the logic" do
    LoansRecorder.record_loan(10_000_000)

    loan_state = :sys.get_state(:loans_recorder)

    assert loan_state.loans_recorded == [10_000_000]

    assert_raise FunctionClauseError, fn ->
      LoansRecorder.record_loan("123")
    end

    assert_raise FunctionClauseError, fn ->
      LoansRecorder.record_loan(nil)
    end
  end

  test "returns changeset for 'identifier'" do
    :sys.replace_state(:loans_recorder, fn state ->
      %{state | loans_changesets: %{"miguel@diaz.com" => %Ecto.Changeset{}}}
    end)

    assert %Ecto.Changeset{} = LoansRecorder.get_loan_changeset_for("miguel@diaz.com")
  end

  test "saves changeset for 'identifier'" do
    LoansRecorder.save_loan_changeset("miguel@dummy.com", %Ecto.Changeset{})

    assert %Ecto.Changeset{} = LoansRecorder.get_loan_changeset_for("miguel@dummy.com")
  end
end
