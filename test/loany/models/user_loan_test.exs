defmodule Loany.Models.UserLoanTest do
  @moduledoc false

  use Loany.DataCase, async: false

  alias Loany.Models.UserLoan

  setup do
    params = %{
      names: "Miguel Diaz",
      email: "dummy@email.com",
      phone_number: "+57123123",
      req_loan_amount: 10000
    }

    start_supervised!(Loany.LoansRecorder, [])

    File.rm("recorded_loans_test")

    {:ok, params: params}
  end

  test "does not accept invalid emails", %{params: params} do
    changeset = UserLoan.changeset(%UserLoan{}, %{params | email: "invalid_emai@gmail"})

    assert %Ecto.Changeset{valid?: false, errors: errors} = changeset

    refute Keyword.get(errors, :email) == nil
  end

  test "inserts interest rate and approval status", %{params: params} do
    changeset = UserLoan.changeset(%UserLoan{}, params)

    assert %Ecto.Changeset{valid?: true, changes: changes} = changeset
    assert %{approval_status: "approved", interest_rate: rate} = changes
    assert rate in Enum.map(4..12, &(&1 / 1))
  end
end
