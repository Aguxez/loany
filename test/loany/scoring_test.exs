defmodule Loany.ScoringTest do
  @moduledoc false

  use Loany.DataCase, async: false

  alias Loany.Scoring

  setup_all do
    File.rm("recorded_loans_test")

    :ok
  end

  setup do
    start_supervised!(Loany.LoansRecorder, [])

    :ok
  end

  test "denies loan request if the amount is the lowest from all the recorded ones" do
    assert {"denied", 0.0} = Scoring.loan_request_status(7)
  end

  test "approves with 9.99 interest if the number is prime" do
    assert {"approved", 9.99} = Scoring.loan_request_status(21_418_373)
  end

  test "approves with a random number between 4 and 12" do
    assert {"approved", interest} = Scoring.loan_request_status(282_731)
    assert interest in Enum.map(4..12, &(&1 / 1))
  end
end
