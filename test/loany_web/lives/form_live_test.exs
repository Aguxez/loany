defmodule LoanyWeb.FormLiveTest do
  @moduledoc false

  use LoanyWeb.ConnCase, async: false

  import Phoenix.LiveViewTest

  alias Loany.Repo
  alias Loany.Models.UserLoan

  setup_all do
    File.rm("recorded_loans_test")

    :ok
  end

  setup do
    payload = %{
      "user_loan" => %{
        names: "Miguel Diaz",
        email: "dummy@email.com",
        phone_number: "+57123123",
        req_loan_amount: 10000,
        interest_rate: 8.0,
        approval_status: "approved"
      }
    }

    start_supervised!(Loany.LoansRecorder, [])

    {:ok, payload: payload}
  end

  test "redirects to offer when saving and saves data to process", %{conn: conn, payload: payload} do
    {:ok, view, html} = live(conn, Routes.live_path(conn, LoanyWeb.FormLive))

    assert html =~ "Request a SEK loan from Loany!"

    path = "/loan_offer?identifier=#{Base.encode64(payload["user_loan"].email)}"

    assert {:error, {:redirect, %{to: ^path}}} = render_submit(view, "save", payload)

    loan_state = :sys.get_state(:loans_recorder)

    assert %{"dummy@email.com" => %Ecto.Changeset{}} = loan_state.loans_changesets
  end

  test "creates loan request even if its denied", %{conn: conn, payload: payload} do
    {:ok, view, _html} = live(conn, Routes.live_path(conn, LoanyWeb.FormLive))

    # Lowest so this should be denied.
    payload = put_in(payload, ["user_loan", :req_loan_amount], 7)

    path = "/loan_offer?redirect=%2F"

    assert_redirect(view, ^path, fn ->
      render_submit(view, "save", payload)
    end)

    assert_remove(view, {:redirect, ^path})

    assert length(Repo.all(UserLoan)) == 1
  end
end
