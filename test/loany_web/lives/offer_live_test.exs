defmodule LoanyWeb.OfferLiveTest do
  @moduledoc false

  use LoanyWeb.ConnCase, async: false

  import Phoenix.LiveViewTest

  alias Loany.Models.UserLoan
  alias Loany.Repo

  setup_all do
    File.rm("recorded_loans_test")

    :ok
  end

  setup do
    payload = %{
      "user_loan" => %{
        approval_status: "approved",
        names: "Miguel Diaz",
        email: "dummy@email.com",
        phone_number: "+57123123",
        req_loan_amount: 10000,
        interest_rate: 9.99
      }
    }

    start_supervised!(Loany.LoansRecorder, [])

    {:ok, payload: payload}
  end

  test "redirects when receiving redirect message", %{conn: conn} do
    {:ok, view, _html} =
      live(
        conn,
        Routes.live_path(conn, LoanyWeb.OfferLive, redirect: "/")
      )

    # manually send it, otherwise we need to wait 3 seconds.
    send(view.pid, {:redirect, "/"})

    assert_remove(view, {:redirect, "/"})
  end

  test "process identifier and saves data when confirming", %{conn: conn, payload: payload} do
    # We're going to manually save the data to process so we don't have to mount FormLive again.
    changeset = Ecto.Changeset.change(%UserLoan{}, payload["user_loan"])
    identifier = Base.encode64(payload["user_loan"].email)

    :sys.replace_state(:loans_recorder, fn state ->
      %{state | loans_changesets: %{payload["user_loan"].email => changeset}}
    end)

    {:ok, view, html} =
      conn
      |> get(Routes.live_path(conn, LoanyWeb.OfferLive, identifier: identifier))
      |> live()

    assert html =~ "Your loan has been partially accepted, you only need to confirm the petition!"
    assert html =~ "<h3>Your loan request amount: <strong>10000</strong><i>SEK</i></h3>"
    assert html =~ "Our interest rate for this loan application: <strong>9.99%</strong>"

    assert_redirect(view, "/", fn ->
      render_submit(view, :loan_confirm)
    end)

    assert_remove(view, {:redirect, "/"})

    assert length(Repo.all(UserLoan)) == 1

    loan_state = :sys.get_state(:loans_recorder)

    assert loan_state.loans_recorded == [payload["user_loan"].req_loan_amount]
  end
end
