defmodule Loany.Scoring do
  @moduledoc """
  Worker in charge of applying scoring's logic to see if a loan request is valid or not.
  """

  alias Loany.LoansRecorder

  def loan_request_status(requested_loan) do
    cond do
      LoansRecorder.is_lowest_loan?(requested_loan) -> {"denied", 0.0}
      is_prime(requested_loan) -> {"approved", 9.99}
      true -> {"approved", 4..12 |> Enum.random() |> Kernel./(1)}
    end
  end

  @doc """
  To check if a number is prime we're applying this test https://en.wikipedia.org/wiki/Primality_test
  Works good for large numbers and if we need to check a single integer instead of a list, for example.
  """
  def is_prime(2) do
    true
  end

  def is_prime(number)
      when rem(number, 2) == 0
      when number <= 1 do
    false
  end

  def is_prime(number) do
    sq_root =
      number
      |> :math.sqrt()
      |> Kernel.trunc()
      |> Kernel.+(1)

    3..sq_root
    |> Stream.take_every(2)
    |> Enum.reduce_while(true, fn num, status ->
      # Preferred to use this because we can stop the iteration whenever we need to and we don't end up
      # making unnecessary calculations.
      if rem(number, num) == 0, do: {:halt, false}, else: {:cont, status}
    end)
  end
end
