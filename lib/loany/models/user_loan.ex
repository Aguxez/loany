defmodule Loany.Models.UserLoan do
  @moduledoc false

  use Ecto.Schema

  import Ecto.Changeset

  alias Ecto.Changeset
  alias Loany.Scoring

  @allowed [:req_loan_amount, :names, :phone_number, :email]
  @required [:req_loan_amount, :names, :phone_number, :email]

  schema "users_loans" do
    field :names, :string
    field :email, :string
    field :phone_number, :string
    field :req_loan_amount, :integer
    field :interest_rate, :float
    field :approval_status, :string

    timestamps()
  end

  @doc false
  def changeset(%__MODULE__{} = user_loan, attrs \\ %{}) do
    user_loan
    |> cast(attrs, @allowed)
    |> validate_required(@required)
    # This RegEx will only match emails with an @ and a dot somewhere after it, super simple.
    |> validate_format(:email, ~r/@.*?\./)
    |> validate_number(:req_loan_amount, greater_than: 1)
    |> set_loan_approval()
  end

  # We save all the loan requests no matter if they were approved or not for review later on.
  defp set_loan_approval(%Changeset{valid?: true} = changeset) do
    {loan_status, interest_rate} =
      changeset
      |> get_change(:req_loan_amount)
      |> Scoring.loan_request_status()

    changeset
    |> put_change(:interest_rate, interest_rate)
    |> put_change(:approval_status, loan_status)
  end

  defp set_loan_approval(changeset) do
    changeset
  end
end
