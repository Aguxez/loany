defmodule Loany.UserLoanInterface do
  @moduledoc false

  import Ecto.Query, only: [from: 2]

  alias Loany.Models.UserLoan
  alias Loany.Repo

  def change_user_loan(params \\ %{}) do
    UserLoan.changeset(%UserLoan{}, params)
  end

  def create_loan_request(%Ecto.Changeset{} = changeset) do
    # Prefer to get the req loan amount from the insert operation, why?
    # It makes the recording of the loan on the GenServer more reliable since
    # we expect the insert operation to succeed. If it doesn't then the
    # loans_recorder process will not go on instead of recording a loan which is not present
    # as the `req_loan_amount` key won't be available.

    with {:ok, loan} <- Repo.insert(changeset),
         _ <- Loany.LoansRecorder.record_loan(loan.req_loan_amount, changeset.changes) do
      {:ok, loan}
    else
      error_changeset -> error_changeset
    end
  end

  def get_all_loans("all") do
    Repo.all(UserLoan)
  end

  def get_all_loans(approval_status) do
    Repo.all(from l in UserLoan, where: l.approval_status == ^approval_status)
  end
end
