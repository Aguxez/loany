defmodule Loany.LoansRecorder do
  @moduledoc false

  use GenServer

  import Ex2ms

  # * NOTE: 'fun' macro comes from Ex2ms to translate functions to ets/dets specifications.

  def start_link(_arg) do
    GenServer.start_link(__MODULE__, %{}, name: :loans_recorder)
  end

  def is_lowest_loan?(requested_loan) do
    GenServer.call(:loans_recorder, {:is_lowest_loan?, requested_loan})
  end

  # Prefer to use a syncrhonous call here because this operation mutates table's state
  # this way we avoid race conditions.
  def record_loan(loan, extra_data \\ %{}) when is_integer(loan) and not is_nil(loan) do
    GenServer.call(:loans_recorder, {:record_loan, loan, extra_data})
  end

  def get_loan_changeset_for(identifier) do
    GenServer.call(:loans_recorder, {:loan_for, identifier})
  end

  def save_loan_changeset(identifier, changeset) do
    GenServer.cast(:loans_recorder, {:save_loan_changeset, identifier, changeset})
  end

  @impl true
  def init(state) do
    {:ok, state, {:continue, :load_data}}
  end

  @impl true
  def handle_continue(:load_data, _state) do
    # we record more data on this but we only care about the loan amount.
    {:ok, table} = :dets.open_file(get_table_name(), type: :set)

    loans_fun =
      fun do
        {loan_amount, _} -> loan_amount
      end

    loans = :dets.select(table, loans_fun)

    :dets.close(table)

    {:noreply, %{table: table, loans_recorded: loans, loans_changesets: %{}}}
  end

  @impl true
  def handle_call({:is_lowest_loan?, requested_loan}, _from, state) do
    current_lowest = Enum.min(state.loans_recorded, fn -> 10 end)

    {:reply, requested_loan < current_lowest, state}
  end

  # We're saving loans to memory because we don't want to hit the disk each time we want to
  # perform a check for a loan. This won't be a problem unless we have a really big dataset on memory.
  @impl true
  def handle_call({:record_loan, loan, extra_data}, _from, state) do
    {:ok, table} = :dets.open_file(state.table)
    :dets.insert(table, {loan, extra_data})
    :dets.close(table)

    {:reply, :ok, %{state | loans_recorded: [loan | state.loans_recorded]}}
  end

  @impl true
  def handle_call({:loan_for, identifier}, _from, state) do
    default = %Ecto.Changeset{valid?: false}

    {:reply, Map.get(state.loans_changesets, identifier, default), state}
  end

  @impl true
  def handle_cast({:save_loan_changeset, identifier, changeset}, state) do
    # this will always replace the 'identifier' value if it already exists.
    new_state = put_in(state, [:loans_changesets], %{identifier => changeset})

    {:noreply, new_state}
  end

  defp get_table_name do
    case Application.get_env(:loany, :env) do
      :test -> :recorded_loans_test
      _ -> :recorded_loans
    end
  end
end
