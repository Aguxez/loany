defmodule LoanyWeb.FormLive do
  @moduledoc false

  use Phoenix.LiveView

  alias Loany.UserLoanInterface
  alias LoanyWeb.Router.Helpers, as: Routes

  @impl true
  def mount(_params, socket) do
    changeset = UserLoanInterface.change_user_loan(%{})

    {:ok, assign(socket, changeset: changeset)}
  end

  @impl true
  def render(assigns) do
    Phoenix.View.render(LoanyWeb.PageView, "index.html", assigns)
  end

  @impl true
  def handle_event("save", %{"user_loan" => params}, socket) do
    case UserLoanInterface.change_user_loan(params) do
      %Ecto.Changeset{valid?: true, changes: %{approval_status: "approved"}} = changeset ->
        encoded_identifier = Base.encode64(params["email"])

        # Puts Loan offer
        updated_socket =
          live_redirect(socket,
            to:
              Routes.live_path(LoanyWeb.Endpoint, LoanyWeb.OfferLive,
                identifier: encoded_identifier
              ),
            replace: true
          )

        Loany.LoansRecorder.save_loan_changeset(params["email"], changeset)

        {:noreply, updated_socket}

      %Ecto.Changeset{valid?: true, changes: %{approval_status: "denied"}} = changeset ->
        # Even if the application was denied we still need to save it but only on the database.
        UserLoanInterface.create_loan_request(changeset)

        updated_socket =
          socket
          |> put_flash(:error, "Try again! You'll be redirected in 3 seconds...")
          |> redirect(to: Routes.live_path(LoanyWeb.Endpoint, LoanyWeb.OfferLive, redirect: "/"))

        {:stop, updated_socket}

      changeset ->
        {:noreply, assign(socket, changeset: Map.put(changeset, :action, :insert))}
    end
  end
end
