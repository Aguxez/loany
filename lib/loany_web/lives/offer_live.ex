defmodule LoanyWeb.OfferLive do
  @moduledoc false

  use Phoenix.LiveView

  alias Loany.{LoansRecorder, UserLoanInterface}

  @impl true
  def mount(_, socket) do
    {:ok, socket}
  end

  @impl true
  def render(assigns) do
    Phoenix.View.render(LoanyWeb.PageView, "loan_offer.html", assigns)
  end

  @impl true
  def handle_params(%{"identifier" => identifier}, _uri, socket) do
    recorded_loan_changeset =
      identifier
      |> Base.decode64!()
      |> LoansRecorder.get_loan_changeset_for()

    interest = Ecto.Changeset.get_change(recorded_loan_changeset, :interest_rate)

    per_month =
      (interest * Ecto.Changeset.get_change(recorded_loan_changeset, :req_loan_amount))
      |> Kernel./(100)
      |> Kernel.trunc()

    {:noreply,
     assign(socket,
       changeset_changes: recorded_loan_changeset.changes,
       changeset: recorded_loan_changeset,
       interest_per_year: per_month * 12,
       sek_per_month: per_month
     )}
  end

  @impl true
  def handle_params(%{"redirect" => path}, _uri, socket) do
    # It's supposed to be 3 but sometimes the socket doesn't connect straight away
    # so we put 2 seconds here to make it quicker internally but on the front it'll look like 3 seconds.
    Process.send_after(self(), {:redirect, path}, 2_000)

    {:noreply, socket}
  end

  @impl true
  def handle_params(_, _uri, socket) do
    {:noreply, socket}
  end

  @impl true
  def handle_info({:redirect, path}, socket) do
    {:stop, redirect(socket, to: path)}
  end

  @impl true
  def handle_event("loan_confirm", _params, socket) do
    case UserLoanInterface.create_loan_request(socket.assigns.changeset) do
      {:ok, loan} ->
        updt_socket =
          socket
          |> put_flash(
            :info,
            "Your loan has been accepted, please refer to this application by the number #{
              loan.id
            }."
          )
          |> redirect(to: "/")

        {:stop, updt_socket}

      {:error, _changeset} ->
        {:noreply, socket}
    end
  end
end
