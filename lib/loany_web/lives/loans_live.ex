defmodule LoanyWeb.LoansLive do
  @moduledoc false

  use Phoenix.LiveView

  alias Loany.UserLoanInterface

  @impl true
  def mount(_, socket) do
    # We're going to return all loans.
    loans = UserLoanInterface.get_all_loans("all")

    {:ok, assign(socket, loans: loans)}
  end

  @impl true
  def render(assigns) do
    Phoenix.View.render(LoanyWeb.PageView, "loans.html", assigns)
  end

  @impl true
  def handle_event("filter_status", %{"loan" => %{"status" => status}}, socket) do
    loans = UserLoanInterface.get_all_loans(status)

    {:noreply, assign(socket, loans: loans)}
  end
end
