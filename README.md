# Loany

## Technical exercise for Bynk interview process.

All the features from the PDF file are included plus the optional one.

* Used Phoenix LiveView for the whole project.
* The accounting team can filter loans by 'approval status'.
* Used DETS for the 'remembering' of the Scoring process.
* After the load request the user needs to confirm their request from the loan offer page and after that is when the process and database save the loan offer.
