defmodule Loany.Repo.Migrations.CreateUsersLoans do
  @moduledoc false

  use Ecto.Migration

  def change do
    create table(:users_loans) do
      add :req_loan_amount, :integer
      add :names, :string
      add :phone_number, :string
      add :email, :string
      add :approval_status, :string
      add :interest_rate, :float

      timestamps()
    end
  end
end
